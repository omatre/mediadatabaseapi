﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MediaDatabaseAPI.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(50)]
        public string Genre { get; set; }
        [Range(1800, 2050)]
        public int ReleaseYear { get; set; }
        [StringLength(1000)]
        public string Description { get; set; } = "";
        [StringLength(50)]
        public string Director { get; set; }
        [Url]
        public string PictureUrl { get; set; }
        [Url]
        public string TrailerUrl { get; set; }
        //FK
        public int FranchiseId { get; set; }
        //Navigation
        public Franchise Franchise { get; set; }
        public ICollection<MovieCharacter> MovieCharacters { get; set; }
    }
}
