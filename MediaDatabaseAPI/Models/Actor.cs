﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MediaDatabaseAPI.Models
{
    public class Actor
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string MiddleName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        [StringLength(20)]
        public string Gender { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        [StringLength(100)]
        public string PlaceOfBirth { get; set; }
        [StringLength(1000)]
        public string Biography { get; set; } = "";
        [Url]
        public string PictureUrl { get; set; }
        public ICollection<MovieCharacter> MovieCharacters { get; set; }
    }
}
