﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace MediaDatabaseAPI.Models
{
    public class Character
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string FullName { get; set; }
        [StringLength(50)]
        public string Alias { get; set; } = "";
        [StringLength(30)]
        public string Gender { get; set; }
        [Url]
        public string PictureUrl { get; set; }
        public ICollection<MovieCharacter> MovieCharacters { get; set; }
    }
}
