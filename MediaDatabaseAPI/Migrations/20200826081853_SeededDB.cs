﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MediaDatabaseAPI.Migrations
{
    public partial class SeededDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "Movies",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Genre",
                table: "Movies",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Director",
                table: "Movies",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Movies",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PictureUrl",
                table: "MovieCharacters",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Franchises",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Franchises",
                maxLength: 1000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Gender",
                table: "Characters",
                maxLength: 30,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FullName",
                table: "Characters",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Alias",
                table: "Characters",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PlaceOfBirth",
                table: "Actors",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MiddleName",
                table: "Actors",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Actors",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Gender",
                table: "Actors",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Actors",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Biography",
                table: "Actors",
                maxLength: 1000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Biography", "DateOfBirth", "FirstName", "Gender", "LastName", "MiddleName", "PictureUrl", "PlaceOfBirth" },
                values: new object[,]
                {
                    { 1, "Joaquin Rafael Phoenix is an American actor, environmentalist, animal rights activist, and producer. He has received numerous awards and nominations, including an Academy Award, a Grammy Award, and two Golden Globe Awards. ", new DateTime(1974, 10, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Joaquin", "Male", "Phoenix", "Rafael", "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Joaquin_Phoenix_in_2018.jpg/800px-Joaquin_Phoenix_in_2018.jpg", "San Juan, Puerto Rico" },
                    { 2, "Robert Anthony De Niro Jr. is an American actor, producer, and director who holds both American and Italian citizenship. He is particularly known for his collaborations with filmmaker Martin Scorsese. He is the recipient of various accolades, including two Academy Awards, a Golden Globe Award, the Cecil B. DeMille Award, and a Screen Actors Guild Life Achievement Award. In 2009, he received the Kennedy Center Honor. In 2016, he received a Presidential Medal of Freedom from U.S. President Barack Obama. ", new DateTime(1943, 8, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Robert", "Male", "De Niro Jr.", "Anthony", "https://upload.wikimedia.org/wikipedia/commons/thumb/5/58/Robert_De_Niro_Cannes_2016.jpg/330px-Robert_De_Niro_Cannes_2016.jpg", "New York City, U.S" },
                    { 3, "Benjamin Géza Affleck-Boldt is an American actor, film director, producer, and screenwriter. His accolades include two Academy Awards and three Golden Globe Awards. He began his career as a child when he starred in the PBS educational series The Voyage of the Mimi (1984, 1988). He later appeared in the independent coming-of-age comedy Dazed and Confused (1993) and various Kevin Smith films, including Mallrats (1995), Chasing Amy (1997) and Dogma (1999). Affleck gained wider recognition when he and childhood friend Matt Damon won the Golden Globe and Academy Award for Best Original Screenplay for writing Good Will Hunting (1997), which they also starred in. He then established himself as a leading man in studio films, including the disaster film Armageddon (1998), the war drama Pearl Harbor (2001), and the thrillers The Sum of All Fears and Changing Lanes (both 2002).", new DateTime(1972, 8, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Benjamin", "Male", "Affleck-Boldt", "Géza", "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Ben_Affleck_by_Gage_Skidmore_3.jpg/330px-Ben_Affleck_by_Gage_Skidmore_3.jpg", "Berkely, California" },
                    { 4, "Henry William Dalgliesh Cavill is a British actor. He is best known for his portrayal of the DC Comics character Superman in the DC Extended Universe, as well as Geralt of Rivia in the Netflix series The Witcher based on the novel series of the same name by Andrzej Sapkowski.", new DateTime(1983, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Henry", "Male", "Cavill", "William Dalgliesh", "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Henry_Cavill_by_Gage_Skidmore_2.jpg/330px-Henry_Cavill_by_Gage_Skidmore_2.jpg", "Saint Helier, Jersey" },
                    { 5, "Christian Charles Philip Bale is an English actor. Bale is the recipient of many awards, including an Academy Award and two Golden Globes, and was featured in the Time 100 list of 2011.", new DateTime(1974, 1, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Christian", "Male", "Bale", "Charles Philip", "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Christian_Bale-7837.jpg/330px-Christian_Bale-7837.jpg", "Haverfordwest, Pembrokeshire, Wales" },
                    { 6, "Heath Andrew Ledger was an Australian actor, photographer and music video director. After performing roles in several Australian television and film productions during the 1990s, Ledger left for the United States in 1998 to further develop his film career. His work consisted of nineteen films, including 10 Things I Hate About You (1999), The Patriot (2000), A Knight's Tale (2001), Monster's Ball (2001), Lords of Dogtown (2005), Brokeback Mountain (2005), Candy (2006), The Dark Knight (2008), and The Imaginarium of Doctor Parnassus (2009), the latter two being posthumous releases.[1] He also produced and directed music videos and aspired to be a film director.", new DateTime(1979, 4, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Heath", "Male", "Ledger", "Andrew", "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Heath_Ledger.jpg/330px-Heath_Ledger.jpg", "Perth, Western Australia" }
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "PictureUrl" },
                values: new object[,]
                {
                    { 1, "Joker", "Arthur Fleck", "Male", "https://upload.wikimedia.org/wikipedia/en/9/98/Joker_%28DC_Comics_character%29.jpg" },
                    { 2, "", "Murray Franklin", "Male", "https://m.media-amazon.com/images/M/MV5BYWRiMjcxYTItYzU4MS00YjNiLWE1ZDktNDFmZmZiYmI0MWYyXkEyXkFqcGdeQXVyNjc0NzQzNTM@._V1_SX1777_CR0,0,1777,821_AL_.jpg" },
                    { 3, "Kent", "Clark", "Male", "https://m.media-amazon.com/images/M/MV5BMTEwOTg1NzQ3ODdeQTJeQWpwZ15BbWU4MDk0OTc1MjIx._V1_UY100_CR16,0,100,100_AL_.jpg" },
                    { 4, "Batman", "Bruce Wayne", "Male", "https://m.media-amazon.com/images/M/MV5BYWVhOGFhZDgtMDdkOS00ODE2LWE2MmItZjc3NzhiNWQyNDk3XkEyXkFqcGdeQXVyMDc2NTEzMw@@._V1_SY1000_CR0,0,1510,1000_AL_.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { 1, "DC Comics, Inc. is an American comic book publisher. It is the publishing unit of DC Entertainment, a subsidiary of the Warner Bros. Global Brands and Experiences division of Warner Bros., a subsidiary of AT&T's WarnerMedia. DC Comics is one of the largest and oldest American comic book companies. The majority of its publications take place within the fictional DC Universe and feature numerous culturally iconic heroic characters, such as Superman, Batman and Wonder Woman. The universe also features well-known supervillains who oppose the superheroes such as Lex Luthor and the Joker. The company has published non-DC Universe-related material, including Watchmen, V for Vendetta, Fables and many titles under their alternative imprint Vertigo.", "DC Comics" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Description", "Director", "FranchiseId", "Genre", "PictureUrl", "ReleaseYear", "Title", "TrailerUrl" },
                values: new object[] { 1, "In Gotham City, mentally troubled comedian Arthur Fleck is disregarded and mistreated by society. He then embarks on a downward spiral of revolution and bloody crime. This path brings him face-to-face with his alter-ego: the Joker.", "Todd Phillips", 1, "Crime, Drama, Thriller", "https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SY1000_CR0,0,674,1000_AL_.jpg", 2019, "Joker", "https://www.imdb.com/video/vi1723318041?playlistId=tt7286456&ref_=tt_ov_vi" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Description", "Director", "FranchiseId", "Genre", "PictureUrl", "ReleaseYear", "Title", "TrailerUrl" },
                values: new object[] { 2, "Fearing that the actions of Superman are left unchecked, Batman takes on the Man of Steel, while the world wrestles with what kind of a hero it really needs.", "Zack Snyder", 1, "Action, Adventure, Sci-Fi", "https://m.media-amazon.com/images/M/MV5BYThjYzcyYzItNTVjNy00NDk0LTgwMWQtYjMwNmNlNWJhMzMyXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg", 2016, "Batman v Superman: Dawn of Justice", "https://www.imdb.com/video/vi1946858521?playlistId=tt2975590&ref_=tt_ov_vi" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Description", "Director", "FranchiseId", "Genre", "PictureUrl", "ReleaseYear", "Title", "TrailerUrl" },
                values: new object[] { 3, "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.", "Christopher Nolan", 1, "Action, Crime, Drama", "https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg", 2008, "The Dark Knight", "https://www.imdb.com/video/vi324468761?playlistId=tt0468569&ref_=tt_ov_vi" });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "ActorId", "CharacterId", "MovieId", "PictureUrl" },
                values: new object[,]
                {
                    { 1, 1, 1, "https://i2.wp.com/p3.no/filmpolitiet/wp-content/uploads/2019/08/Joker-bilde-6.jpeg?resize=1150%2C647&ssl=1" },
                    { 2, 2, 1, "https://m.media-amazon.com/images/M/MV5BYWRiMjcxYTItYzU4MS00YjNiLWE1ZDktNDFmZmZiYmI0MWYyXkEyXkFqcGdeQXVyNjc0NzQzNTM@._V1_SX1777_CR0,0,1777,821_AL_.jpg" },
                    { 3, 4, 2, "https://m.media-amazon.com/images/M/MV5BMTg2NzE5NzQ0MF5BMl5BanBnXkFtZTgwODkyMzQ0NTE@._V1_UY100_CR0,0,100,100_AL_.jpg" },
                    { 4, 3, 2, "https://m.media-amazon.com/images/M/MV5BMTQ2MTgyNzY2MV5BMl5BanBnXkFtZTgwOTA5MjkxNjE@._V1_UY99_CR25,0,99,99_AL_.jpg" },
                    { 6, 1, 3, "https://m.media-amazon.com/images/M/MV5BMjA5ODU3NTI0Ml5BMl5BanBnXkFtZTcwODczMTk2Mw@@._V1_UY100_CR67,0,100,100_AL_.jpg" },
                    { 5, 4, 3, "https://m.media-amazon.com/images/M/MV5BMTkyNTI0NDM5NF5BMl5BanBnXkFtZTcwMDkzMTk2Mw@@._V1_UY100_CR67,0,100,100_AL_.jpg" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "ActorId", "CharacterId", "MovieId" },
                keyValues: new object[] { 1, 1, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "ActorId", "CharacterId", "MovieId" },
                keyValues: new object[] { 2, 2, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "ActorId", "CharacterId", "MovieId" },
                keyValues: new object[] { 3, 4, 2 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "ActorId", "CharacterId", "MovieId" },
                keyValues: new object[] { 4, 3, 2 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "ActorId", "CharacterId", "MovieId" },
                keyValues: new object[] { 5, 4, 3 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "ActorId", "CharacterId", "MovieId" },
                keyValues: new object[] { 6, 1, 3 });

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Movies");

            migrationBuilder.DropColumn(
                name: "PictureUrl",
                table: "MovieCharacters");

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "Movies",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Genre",
                table: "Movies",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Director",
                table: "Movies",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Franchises",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Franchises",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 1000,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Gender",
                table: "Characters",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 30,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FullName",
                table: "Characters",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Alias",
                table: "Characters",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PlaceOfBirth",
                table: "Actors",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MiddleName",
                table: "Actors",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Actors",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Gender",
                table: "Actors",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Actors",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Biography",
                table: "Actors",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 1000,
                oldNullable: true);
        }
    }
}
