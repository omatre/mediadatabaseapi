﻿using AutoMapper;
using MediaDBAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaDBAPI.DTO.FranchiseDTO
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseDTO>().ReverseMap();
        }
    }
}
