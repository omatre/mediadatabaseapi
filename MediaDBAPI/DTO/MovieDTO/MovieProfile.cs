﻿using AutoMapper;
using MediaDBAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaDBAPI.DTO.MovieDTO
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>().ForMember(mdto => mdto.FranchiseName, opt => opt.MapFrom(m => m.Franchise.Name));
        }
    }
}
