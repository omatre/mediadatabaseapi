﻿using AutoMapper;
using MediaDBAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaDBAPI.DTO.MovieDTO
{
    public class MovieWithoutFranchiseProfile : Profile
    {
        public MovieWithoutFranchiseProfile()
        {
            CreateMap<Movie, MovieWithoutFranchiseDTO>().ReverseMap();
        }
    }
}
