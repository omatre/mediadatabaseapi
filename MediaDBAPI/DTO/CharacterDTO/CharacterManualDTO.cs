﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaDBAPI.DTO.CharacterDTO
{
    public class CharacterManualDTO
    {
        public string FullName { get; set; }
        public string Alias { get; set; } = "";
        public string Gender { get; set; }
    }
}
