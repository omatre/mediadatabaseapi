﻿using MediaDBAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaDBAPI.DTO.MovieCharacterDTO
{
    public class MovieCharacterActorDto
    {
        public string PictureUrl { get; set; }
        //Movie info
        public string MovieTitle { get; set; }
        //Character info
        public string CharacterFullName { get; set; }
        public string CharacterAlias { get; set; }
        public string CharacterGender { get; set; }
        //Actor info
        public string ActorFirstName { get; set; }
        public string ActorMiddleName { get; set; }
        public string ActorLastName { get; set; }
    }
}
