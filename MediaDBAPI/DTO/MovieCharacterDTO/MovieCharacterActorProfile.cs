﻿using AutoMapper;
using MediaDBAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaDBAPI.DTO.MovieCharacterDTO
{
    public class MovieCharacterActorProfile : Profile
    {
        public MovieCharacterActorProfile()
        {
            CreateMap<MovieCharacter, MovieCharacterActorDto>().IncludeMembers(mcdto => mcdto.Movie, mcdto => mcdto.Character, mcdto => mcdto.Actor);

            CreateMap<Movie, MovieCharacterActorDto>().IncludeMembers(m => m.Title);
            CreateMap<Character, MovieCharacterActorDto>().IncludeMembers(c => c.Alias, c => c.FullName, c => c.Gender);
            CreateMap<Actor, MovieCharacterActorDto>().IncludeMembers(a => a.FirstName, a => a.MiddleName, a => a.LastName);

            CreateMap<string, MovieCharacterActorDto>().ForAllMembers(opt => opt.MapFrom(x => x.ToString()));
        }
    }
}
