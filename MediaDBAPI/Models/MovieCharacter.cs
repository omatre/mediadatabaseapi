﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MediaDBAPI.Models
{
    public class MovieCharacter
    {
        [Url]
        public string PictureUrl { get; set; }
        //FKs
        public int MovieId { get; set; }
        public int CharacterId { get; set; }
        public int ActorId { get; set; }

        //Navigation
        public Movie Movie { get; set; }
        public Character Character { get; set; }
        public Actor Actor { get; set; }
    }
}
