﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaDBAPI.Models
{
    public class MediaDbContext : DbContext
    {
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }

        public MediaDbContext(DbContextOptions<MediaDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieCharacter>()
                .HasKey(mc => new { mc.ActorId, mc.CharacterId, mc.MovieId});

            modelBuilder.Entity<MovieCharacter>()
                .HasOne(mc => mc.Character)
                .WithMany(c => c.MovieCharacters)
                .HasForeignKey(mc => mc.CharacterId);

            modelBuilder.Entity<MovieCharacter>()
                .HasOne(mc => mc.Movie)
                .WithMany(m => m.MovieCharacters)
                .HasForeignKey(mc => mc.MovieId);

            modelBuilder.Entity<MovieCharacter>()
                .HasOne(mc => mc.Actor)
                .WithMany(a => a.MovieCharacters)
                .HasForeignKey(mc => mc.ActorId);


            modelBuilder.Entity<Actor>()
                .HasData(
                    new Actor
                    {
                        Id = 1,
                        FirstName = "Joaquin",
                        LastName = "Phoenix",
                        MiddleName = "Rafael",
                        Gender = "Male",
                        DateOfBirth = new DateTime(1974, 10, 28),
                        PlaceOfBirth = "San Juan, Puerto Rico",
                        Biography = "Joaquin Rafael Phoenix is an American actor, environmentalist, animal rights activist, " +
                        "and producer. He has received numerous awards and nominations, including an Academy Award, a Grammy " +
                        "Award, and two Golden Globe Awards. ",
                        PictureUrl = @"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Joaquin_Phoenix_in_2018.jpg/800px-Joaquin_Phoenix_in_2018.jpg"
                    }
                );

            modelBuilder.Entity<Actor>()
                .HasData(
                    new Actor
                    {
                        Id = 2,
                        FirstName = "Robert",
                        LastName = "De Niro Jr.",
                        MiddleName = "Anthony",
                        Gender = "Male",
                        DateOfBirth = new DateTime(1943, 8, 17),
                        PlaceOfBirth = "New York City, U.S",
                        Biography = "Robert Anthony De Niro Jr. is an American actor, producer, and director who " +
                        "holds both American and Italian citizenship. He is particularly known for his collaborations " +
                        "with filmmaker Martin Scorsese. He is the recipient of various accolades, including two " +
                        "Academy Awards, a Golden Globe Award, the Cecil B. DeMille Award, and a Screen Actors Guild " +
                        "Life Achievement Award. In 2009, he received the Kennedy Center Honor. In 2016, he received " +
                        "a Presidential Medal of Freedom from U.S. President Barack Obama. ",
                        PictureUrl = @"https://upload.wikimedia.org/wikipedia/commons/thumb/5/58/Robert_De_Niro_Cannes_2016.jpg/330px-Robert_De_Niro_Cannes_2016.jpg"
                    }
                );

            modelBuilder.Entity<Actor>()
                .HasData(
                    new Actor
                    {
                        Id = 3,
                        FirstName = "Benjamin",
                        LastName = "Affleck-Boldt",
                        MiddleName = "Géza",
                        Gender = "Male",
                        DateOfBirth = new DateTime(1972, 8, 15),
                        PlaceOfBirth = "Berkely, California",
                        Biography = "Benjamin Géza Affleck-Boldt is an American actor, film director, producer, " +
                        "and screenwriter. His accolades include two Academy Awards and three Golden Globe Awards. " +
                        "He began his career as a child when he starred in the PBS educational series The Voyage of " +
                        "the Mimi (1984, 1988). He later appeared in the independent coming-of-age comedy Dazed and " +
                        "Confused (1993) and various Kevin Smith films, including Mallrats (1995), Chasing Amy (1997) " +
                        "and Dogma (1999). Affleck gained wider recognition when he and childhood friend Matt Damon won " +
                        "the Golden Globe and Academy Award for Best Original Screenplay for writing Good Will Hunting " +
                        "(1997), which they also starred in. He then established himself as a leading man in studio films, " +
                        "including the disaster film Armageddon (1998), the war drama Pearl Harbor (2001), and the thrillers " +
                        "The Sum of All Fears and Changing Lanes (both 2002).",
                        PictureUrl = @"https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Ben_Affleck_by_Gage_Skidmore_3.jpg/330px-Ben_Affleck_by_Gage_Skidmore_3.jpg"
                    }
                );

            modelBuilder.Entity<Actor>()
                .HasData(
                    new Actor
                    {
                        Id = 4,
                        FirstName = "Henry",
                        LastName = "Cavill",
                        MiddleName = "William Dalgliesh",
                        Gender = "Male",
                        DateOfBirth = new DateTime(1983, 5, 5),
                        PlaceOfBirth = "Saint Helier, Jersey",
                        Biography = "Henry William Dalgliesh Cavill is a British actor. He is best known " +
                        "for his portrayal of the DC Comics character Superman in the DC Extended Universe, " +
                        "as well as Geralt of Rivia in the Netflix series The Witcher based on the novel " +
                        "series of the same name by Andrzej Sapkowski.",
                        PictureUrl = @"https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Henry_Cavill_by_Gage_Skidmore_2.jpg/330px-Henry_Cavill_by_Gage_Skidmore_2.jpg"
                    }
                );

            modelBuilder.Entity<Actor>()
                .HasData(
                    new Actor
                    {
                        Id = 5,
                        FirstName = "Christian",
                        LastName = "Bale",
                        MiddleName = "Charles Philip",
                        Gender = "Male",
                        DateOfBirth = new DateTime(1974, 1, 30),
                        PlaceOfBirth = "Haverfordwest, Pembrokeshire, Wales",
                        Biography = "Christian Charles Philip Bale is an English actor. Bale is the recipient " +
                        "of many awards, including an Academy Award and two Golden Globes, and was featured in " +
                        "the Time 100 list of 2011.",
                        PictureUrl = @"https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Christian_Bale-7837.jpg/330px-Christian_Bale-7837.jpg"
                    }
                );

            modelBuilder.Entity<Actor>()
                .HasData(
                    new Actor
                    {
                        Id = 6,
                        FirstName = "Heath",
                        LastName = "Ledger",
                        MiddleName = "Andrew",
                        Gender = "Male",
                        DateOfBirth = new DateTime(1979, 4, 4),
                        PlaceOfBirth = "Perth, Western Australia",
                        Biography = "Heath Andrew Ledger was an Australian actor, photographer and music video " +
                        "director. After performing roles in several Australian television and film productions " +
                        "during the 1990s, Ledger left for the United States in 1998 to further develop his film " +
                        "career. His work consisted of nineteen films, including 10 Things I Hate About You (1999), " +
                        "The Patriot (2000), A Knight's Tale (2001), Monster's Ball (2001), Lords of Dogtown (2005), " +
                        "Brokeback Mountain (2005), Candy (2006), The Dark Knight (2008), and The Imaginarium of Doctor " +
                        "Parnassus (2009), the latter two being posthumous releases.[1] He also produced and directed " +
                        "music videos and aspired to be a film director.",
                        PictureUrl = @"https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Heath_Ledger.jpg/330px-Heath_Ledger.jpg"
                    }
                );

            //modelBuilder.Entity<Actor>()
            //    .HasData(
            //        new Actor
            //        {
            //            FirstName = "",
            //            LastName = "",
            //            MiddleName = "",
            //            Gender = "",
            //            DateOfBirth = new DateTime(),
            //            PlaceOfBirth = "",
            //            Biography = "",
            //            PictureUrl = ""
            //        }
            //    );

            modelBuilder.Entity<Character>()
                .HasData(
                    new Character
                    {
                        Id = 1,
                        FullName = "Arthur Fleck",
                        Alias = "Joker",
                        Gender = "Male",
                        PictureUrl = @"https://upload.wikimedia.org/wikipedia/en/9/98/Joker_%28DC_Comics_character%29.jpg"
                    }
                );

            modelBuilder.Entity<Character>()
                .HasData(
                    new Character
                    {
                        Id = 2,
                        FullName = "Murray Franklin",
                        Alias = "",
                        Gender = "Male",
                        PictureUrl = @"https://m.media-amazon.com/images/M/MV5BYWRiMjcxYTItYzU4MS00YjNiLWE1ZDktNDFmZmZiYmI0MWYyXkEyXkFqcGdeQXVyNjc0NzQzNTM@._V1_SX1777_CR0,0,1777,821_AL_.jpg"
                    }
                );

            modelBuilder.Entity<Character>()
                .HasData(
                    new Character
                    {
                        Id = 3,
                        FullName = "Clark Kent",
                        Alias = "Superman",
                        Gender = "Male",
                        PictureUrl = @"https://m.media-amazon.com/images/M/MV5BMTEwOTg1NzQ3ODdeQTJeQWpwZ15BbWU4MDk0OTc1MjIx._V1_UY100_CR16,0,100,100_AL_.jpg"
                    }
                );

            modelBuilder.Entity<Character>()
                .HasData(
                    new Character
                    {
                        Id = 4,
                        FullName = "Bruce Wayne",
                        Alias = "Batman",
                        Gender = "Male",
                        PictureUrl = @"https://m.media-amazon.com/images/M/MV5BYWVhOGFhZDgtMDdkOS00ODE2LWE2MmItZjc3NzhiNWQyNDk3XkEyXkFqcGdeQXVyMDc2NTEzMw@@._V1_SY1000_CR0,0,1510,1000_AL_.jpg"
                    }
                );

            //modelBuilder.Entity<Character>()
            //    .HasData(
            //        new Character
            //        {
            //            FullName = "",
            //            Alias = "",
            //            Gender = "",
            //            PictureUrl = ""
            //        }
            //    );

            modelBuilder.Entity<Franchise>()
                .HasData(
                    new Franchise
                    {
                        Id = 1,
                        Name = "DC Comics",
                        Description = "DC Comics, Inc. is an American comic book publisher. It is the publishing " +
                        "unit of DC Entertainment, a subsidiary of the Warner Bros. Global Brands and Experiences " +
                        "division of Warner Bros., a subsidiary of AT&T's WarnerMedia. DC Comics is one of the largest " +
                        "and oldest American comic book companies. The majority of its publications take place within " +
                        "the fictional DC Universe and feature numerous culturally iconic heroic characters, such as " +
                        "Superman, Batman and Wonder Woman. The universe also features well-known supervillains who oppose " +
                        "the superheroes such as Lex Luthor and the Joker. The company has published non-DC Universe-related " +
                        "material, including Watchmen, V for Vendetta, Fables and many titles under their alternative imprint Vertigo."
                    }
                );

            //modelBuilder.Entity<Franchise>()
            //    .HasData(
            //        new Franchise
            //        {
            //            Name = "",
            //            Description = ""
            //        }
            //    );

            modelBuilder.Entity<Movie>()
                .HasData(
                    new Movie
                    {
                        Id = 1,
                        Title = "Joker",
                        Genre = "Crime, Drama, Thriller",
                        ReleaseYear = 2019,
                        Description = "In Gotham City, mentally troubled comedian Arthur Fleck is disregarded and " +
                        "mistreated by society. He then embarks on a downward spiral of revolution and bloody crime. " +
                        "This path brings him face-to-face with his alter-ego: the Joker.",
                        Director = "Todd Phillips",
                        PictureUrl = @"https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SY1000_CR0,0,674,1000_AL_.jpg",
                        TrailerUrl = @"https://www.imdb.com/video/vi1723318041?playlistId=tt7286456&ref_=tt_ov_vi",
                        FranchiseId = 1
                    }
                );

            modelBuilder.Entity<Movie>()
                .HasData(
                    new Movie
                    {
                        Id = 2,
                        Title = "Batman v Superman: Dawn of Justice",
                        Genre = "Action, Adventure, Sci-Fi",
                        ReleaseYear = 2016,
                        Description = "Fearing that the actions of Superman are left unchecked, Batman " +
                        "takes on the Man of Steel, while the world wrestles with what kind of a hero it really needs.",
                        Director = "Zack Snyder",
                        PictureUrl = @"https://m.media-amazon.com/images/M/MV5BYThjYzcyYzItNTVjNy00NDk0LTgwMWQtYjMwNmNlNWJhMzMyXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg",
                        TrailerUrl = @"https://www.imdb.com/video/vi1946858521?playlistId=tt2975590&ref_=tt_ov_vi",
                        FranchiseId = 1
                    }
                );

            modelBuilder.Entity<Movie>()
                .HasData(
                    new Movie
                    {
                        Id = 3,
                        Title = "The Dark Knight",
                        Genre = "Action, Crime, Drama",
                        ReleaseYear = 2008,
                        Description = "When the menace known as the Joker wreaks havoc and chaos on the " +
                        "people of Gotham, Batman must accept one of the greatest psychological and " +
                        "physical tests of his ability to fight injustice.",
                        Director = "Christopher Nolan",
                        PictureUrl = @"https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg",
                        TrailerUrl = @"https://www.imdb.com/video/vi324468761?playlistId=tt0468569&ref_=tt_ov_vi",
                        FranchiseId = 1
                    }
                );

            //modelBuilder.Entity<Movie>()
            //    .HasData(
            //        new Movie
            //        {
            //            Title = "",
            //            Genre = "",
            //            ReleaseYear = 2000,
            //            Description = "",
            //            Director = "",
            //            PictureUrl = "",
            //            TrailerUrl = "",
            //            FranchiseId = 0
            //        }
            //    );

            modelBuilder.Entity<MovieCharacter>()
                .HasData(
                    new MovieCharacter
                    {
                        MovieId = 1,
                        CharacterId = 1,
                        ActorId = 1,
                        PictureUrl = @"https://i2.wp.com/p3.no/filmpolitiet/wp-content/uploads/2019/08/Joker-bilde-6.jpeg?resize=1150%2C647&ssl=1"
                    }
                );

            modelBuilder.Entity<MovieCharacter>()
                .HasData(
                    new MovieCharacter
                    {
                        MovieId = 1,
                        CharacterId = 2,
                        ActorId = 2,
                        PictureUrl = @"https://m.media-amazon.com/images/M/MV5BYWRiMjcxYTItYzU4MS00YjNiLWE1ZDktNDFmZmZiYmI0MWYyXkEyXkFqcGdeQXVyNjc0NzQzNTM@._V1_SX1777_CR0,0,1777,821_AL_.jpg"
                    }
                );

            modelBuilder.Entity<MovieCharacter>()
                .HasData(
                    new MovieCharacter
                    {
                        MovieId = 2,
                        CharacterId = 4,
                        ActorId = 3,
                        PictureUrl = @"https://m.media-amazon.com/images/M/MV5BMTg2NzE5NzQ0MF5BMl5BanBnXkFtZTgwODkyMzQ0NTE@._V1_UY100_CR0,0,100,100_AL_.jpg"
                    }
                );

            modelBuilder.Entity<MovieCharacter>()
                .HasData(
                    new MovieCharacter
                    {
                        MovieId = 2,
                        CharacterId = 3,
                        ActorId = 4,
                        PictureUrl = @"https://m.media-amazon.com/images/M/MV5BMTQ2MTgyNzY2MV5BMl5BanBnXkFtZTgwOTA5MjkxNjE@._V1_UY99_CR25,0,99,99_AL_.jpg"
                    }
                );

            modelBuilder.Entity<MovieCharacter>()
                .HasData(
                    new MovieCharacter
                    {
                        MovieId = 3,
                        CharacterId = 1,
                        ActorId = 6,
                        PictureUrl = @"https://m.media-amazon.com/images/M/MV5BMjA5ODU3NTI0Ml5BMl5BanBnXkFtZTcwODczMTk2Mw@@._V1_UY100_CR67,0,100,100_AL_.jpg"
                    }
                );

            modelBuilder.Entity<MovieCharacter>()
                .HasData(
                    new MovieCharacter
                    {
                        MovieId = 3,
                        CharacterId = 4,
                        ActorId = 5,
                        PictureUrl = @"https://m.media-amazon.com/images/M/MV5BMTkyNTI0NDM5NF5BMl5BanBnXkFtZTcwMDkzMTk2Mw@@._V1_UY100_CR67,0,100,100_AL_.jpg"
                    }
                );

            //modelBuilder.Entity<MovieCharacter>()
            //    .HasData(
            //        new MovieCharacter
            //        {
            //            MovieId = 0,
            //            CharacterId = 0,
            //            ActorId = 0,
            //            PictureUrl = ""
            //        }
            //    );
        }
    }
}
