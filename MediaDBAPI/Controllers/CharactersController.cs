﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MediaDBAPI.Models;
using MediaDBAPI.DTO.CharacterDTO;
using AutoMapper;
using MediaDBAPI.DTO.ActorDTO;

namespace MediaDBAPI.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MediaDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MediaDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Characters
        /// <summary>
        /// Gets all characters in database.
        /// </summary>
        /// <returns>List of characters</returns>
        [HttpGet]
        public async IAsyncEnumerable<ActionResult<CharacterManualDTO>> GetAll()
        {
            var characters = await _context.Characters.ToListAsync();

            if (characters == null)
            {
                yield return NotFound();
            }

            foreach (var character in characters)
            {
                var characterDTO = new CharacterManualDTO
                {
                    FullName = character.FullName,
                    Alias = character.Alias,
                    Gender = character.Gender
                };

                yield return characterDTO;
            }
        }

        // GET: api/Characters/5
        /// <summary>
        /// Gets character with certain id.
        /// </summary>
        /// <param name="id">Id of character</param>
        /// <returns>A character</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDTO>> Details(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<Character, CharacterDTO>(character);
        }

        // GET: api/Characters/5/actors
        /// <summary>
        /// Gets all actors that have played a character with certain id.
        /// </summary>
        /// <param name="id">Id of character</param>
        /// <returns>List of actors</returns>
        [HttpGet("{id}/actors")]
        public async Task<ActionResult<IEnumerable<ActorDTO>>> Actors(int id)
        {
            var actors = await _context.MovieCharacters
                                       .Where(mc => mc.CharacterId == id)
                                       .Select(mc => mc.Actor)
                                       .ToListAsync();

            if (actors == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<Actor>, List<ActorDTO>>(actors);
        }

        // PUT: api/Characters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, Character character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }

        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Character>> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return character;
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
