﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MediaDBAPI.Models;
using System.ComponentModel;
using AutoMapper;
using MediaDBAPI.DTO.FranchiseDTO;
using MediaDBAPI.DTO.CharacterDTO;
using MediaDBAPI.DTO.MovieDTO;

namespace MediaDBAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MediaDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MediaDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Franchises
        /// <summary>
        /// Gets all franchises in database.
        /// </summary>
        /// <returns>List of franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetAll()
        {
            var franchises = await _context.Franchises.ToListAsync();

            if (franchises == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<Franchise>, List<FranchiseDTO>>(franchises);
        }

        // GET: api/Franchises/5
        /// <summary>
        /// Gets franchise with certain id.
        /// </summary>
        /// <param name="id">Id of franchise</param>
        /// <returns>A franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> Details(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<Franchise, FranchiseDTO>(franchise);
        }

        // GET: api/Franchises/5/movies
        /// <summary>
        /// Gets all movies under franchise with certain id.
        /// </summary>
        /// <param name="id">Id of franchise</param>
        /// <returns>List of movies</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> Movies(int id)
        {
            var movies = await _context.Movies
                                       .Where(m => m.FranchiseId == id)
                                       .Include(m => m.Franchise)
                                       .ToListAsync();

            if (movies == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<Movie>, List<MovieDTO>>(movies);
        }

        // GET: api/Franchises/5/characters
        /// <summary>
        /// Gets all characters in a franchise with certain id.
        /// </summary>
        /// <param name="id">Id of franchise</param>
        /// <returns>List of characters</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> Characters(int id)
        {
            var characters = await _context.Franchises
                                           .Where(f => f.Id == id)
                                           .SelectMany(f => f.Movies)
                                           .SelectMany(m => m.MovieCharacters)
                                           .Select(mc => mc.Character)
                                           .Distinct()
                                           .ToListAsync();

            if (characters == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<Character>, List<CharacterDTO>>(characters);
        }

        // PUT: api/Franchises/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise([FromBody]FranchiseDTO franchiseDTO)
        {
            var franchise = new Franchise
            {
                Name = franchiseDTO.Name,
                Description = franchiseDTO.Description
            };

            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(Details), new { id = franchise.Id }, franchise);
        }

        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Franchise>> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
