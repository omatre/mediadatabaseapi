﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MediaDBAPI.Models;
using AutoMapper;
using MediaDBAPI.DTO.MovieCharacterDTO;
using MediaDBAPI.DTO.MovieDTO;

namespace MediaDBAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MediaDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MediaDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Movies
        /// <summary>
        /// Gets all movies in database.
        /// </summary>
        /// <returns>List of movies</returns>
        [HttpGet]
        public async IAsyncEnumerable<ActionResult<MovieManualDTO>> GetAll()
        {
            //TODO: Return each element as result=null and value=movieprops
            var movies = await _context.Movies.ToListAsync();

            if (movies == null)
            {
                yield return NotFound();
            }

            foreach (var movie in movies)
            {
                var movieDTO = new MovieManualDTO
                {
                    Title = movie.Title,
                    Genre = movie.Genre,
                    ReleaseYear = movie.ReleaseYear
                };

                yield return movieDTO;
            }
        }

        // GET: api/Movies/5
        /// <summary>
        /// Gets movie with certain id.
        /// </summary>
        /// <param name="id">Id of movie</param>
        /// <returns>A movie</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> Details(int id)
        {
            var movie = await _context.Movies.Include(m => m.Franchise).FirstOrDefaultAsync(m => m.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<Movie, MovieDTO>(movie);
        }

        // GET: api/Movies/5/characters-with-actor-info
        /// <summary>
        /// Gets all characters in movie with certain id including actor.
        /// </summary>
        /// <param name="id">Id of movie</param>
        /// <returns>List of characters</returns>
        [HttpGet("{id}/characters-with-actor-info")]
        public async Task<ActionResult<IEnumerable<MovieCharacterActorDto>>> CharactersFromMovie(int id)
        {
            var movieCharacter = await _context.MovieCharacters
                                               .Where(mc => mc.MovieId == id)
                                               .Include(mc => mc.Movie)
                                               .Include(mc => mc.Character)
                                               .Include(mc => mc.Actor)
                                               .ToListAsync();

            if (movieCharacter == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<MovieCharacter>, List<MovieCharacterActorDto>>(movieCharacter);
        }



        // PUT: api/Movies/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Movies
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Movie>> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return movie;
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
