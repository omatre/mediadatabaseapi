﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MediaDBAPI.DTO.ActorDTO;
using AutoMapper;
using MediaDBAPI.DTO.CharacterDTO;
using MediaDBAPI.DTO.MovieDTO;
using MediaDBAPI.Models;

namespace MediaDBAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly MediaDbContext _context;
        private readonly IMapper _mapper;

        public ActorsController(MediaDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Actors
        /// <summary>
        /// Gets all actors in the database.
        /// </summary>
        /// <returns>List of actors</returns>
        [HttpGet]
        public async IAsyncEnumerable<ActionResult<ActorManualDTO>> GetAll()
        {
            var actors = await _context.Actors.ToListAsync();

            if (actors == null)
            {
                yield return NotFound();
            }

            foreach (var actor in actors)
            {
                var actorDTO = new ActorManualDTO
                {
                    FirstName = actor.FirstName,
                    MiddleName = actor.MiddleName,
                    LastName = actor.LastName
                };

                yield return actorDTO;
            }
        }

        // GET: api/Actors/5
        /// <summary>
        /// Gets an actor with certain id.
        /// </summary>
        /// <param name="id">Id of actor</param>
        /// <returns>An actor</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorDTO>> Details(int id)
        {
            var actor = await _context.Actors.FindAsync(id);

            if (actor == null)
            {
                return NotFound();
            }

            return _mapper.Map<Actor, ActorDTO>(actor);
        }

        // GET: api/Actors/5/characters
        /// <summary>
        /// Gets all the characters played by an actor with certain id.
        /// </summary>
        /// <param name="id">Id of actor</param>
        /// <returns>List of characters</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> Characters(int id)
        {
            var characters = await _context.MovieCharacters
                                           .Where(mc => mc.ActorId == id)
                                           .Select(mc => mc.Character)
                                           .ToListAsync();

            if (characters == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<Character>, List<CharacterDTO>>(characters);
        }

        // GET: api/Actors/5/movies
        /// <summary>
        /// Gets all movies featuring an actor with certain id.
        /// </summary>
        /// <param name="id">Id of actor</param>
        /// <returns>List of movies</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieWithoutFranchiseDTO>>> Movies(int id)
        {
            var movies = await _context.MovieCharacters
                                       .Where(mc => mc.ActorId == id)
                                       .Select(mc => mc.Movie)
                                       .ToListAsync();

            if (movies == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<Movie>, List<MovieWithoutFranchiseDTO>>(movies);
        }

        // PUT: api/Actors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Actors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Actor>> PostActor(Actor actor)
        {
            _context.Actors.Add(actor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActor", new { id = actor.Id }, actor);
        }

        // DELETE: api/Actors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Actor>> DeleteActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            return actor;
        }

        private bool ActorExists(int id)
        {
            return _context.Actors.Any(e => e.Id == id);
        }
    }
}
