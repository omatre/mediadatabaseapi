﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MediaDBAPI.Migrations
{
    public partial class DockerContainerDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Alias", "FullName" },
                values: new object[] { "Superman", "Clark Kent" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Alias", "FullName" },
                values: new object[] { "Kent", "Clark" });
        }
    }
}
